From c624c9dfe03c0e066eea1240b4b9ca8f3ed07eb8 Mon Sep 17 00:00:00 2001
From: John Chung <john.chung@arm.com>
Date: Fri, 26 May 2023 22:03:50 +0800
Subject: [PATCH] Redfish-finder cannot work in DHCP mode

dmidecode output didn't provide Redfish Service Address in dhcp mode.
It will fail to match domain name to ip address. Move to fetch DHCP
server ip address from nmcli output.

dmidecode output :

Handle 0x0021, DMI type 42, 129 bytes
Management Controller Host Interface
        Host Interface Type: Network
        Device Type: USB
        idVendor: 0x0b05
        idProduct: 0x1976
        Protocol ID: 04 (Redfish over IP)
                Service UUID: 24913078-eeb7-5842-b08b-732425cc09ea
                Host IP Assignment Type: DHCP
                Host IP Address Format: IPv4
                Redfish Service IP Discovery Type: DHCP
                Redfish Service IP Address Format: IPv4
                Redfish Service Hostname: bmc.host.interface

Signed-off-by: John Chung <john.chung@arm.com>
---
 redfish-finder | 43 +++++++++++++++++++++++++++++++++++--------
 1 file changed, 35 insertions(+), 8 deletions(-)

diff --git a/redfish-finder b/redfish-finder
index 461eff9..b869a9b 100755
--- a/redfish-finder
+++ b/redfish-finder
@@ -203,6 +203,7 @@ class ServiceConfig():
 	def __init__(self, cursor):
 		self.address = []
 		self.mask = []
+		self.dhcp_format = []
 		try:
 			cursor = cursor_consume_next(cursor, "Redfish Service IP Discovery Type: ")
 			if cursor == None:
@@ -220,16 +221,21 @@ class ServiceConfig():
 					self.address.append(ipaddress.IPv6Address(unicode(cursor.split()[0], "utf-8")))
 					cursor = cursor_consume_next(cursor, "IPv6 Mask: ")
 					self.mask.append(ipaddress.IPv4Address(unicode(cursor.split()[0], "utf-8")))
+				cursor = cursor_consume_next(cursor, "Redfish Service Port: ")
+				self.port = int(cursor.split()[0])
+				cursor = cursor_consume_next(cursor, "Redfish Service Vlan: ")
+				self.vlan = int(cursor.split()[0])
 			elif cursor.split()[0] == "DHCP":
 				self.assigntype = AssignType.DHCP
+				cursor = cursor_consume_next(cursor, "Redfish Service IP Address Format: ")
+				if cursor.split()[0] == "IPv4":
+					self.dhcp_format.append("DHCP4")
+				elif cursor.split()[0] == "IPv6":
+					self.dhcp_format.append("DHCP6")
 			else:
 				# Support the other types later
 				print("redfish-finder: Unable to parse SMBIOS Service Config info")
 				return None
-			cursor = cursor_consume_next(cursor, "Redfish Service Port: ")
-			self.port = int(cursor.split()[0])
-			cursor = cursor_consume_next(cursor, "Redfish Service Vlan: ")
-			self.vlan = int(cursor.split()[0])
 			cursor = cursor_consume_next(cursor, "Redfish Service Hostname: ")
 
 			#
@@ -338,7 +344,7 @@ class OSServiceData():
 	# Method to read in /etc/hosts, remove old redfish entries
 	# and insert new ones based on ServiceConfig
 	#
-	def update_redfish_info(self):
+	def update_redfish_info(self, conn):
 		# strip any redfish localhost entry from host_entries
 		# as well as any entries for the smbios exported host name
 		for h in self.host_entries:
@@ -351,8 +357,14 @@ class OSServiceData():
 
 		# Now add the new entries in
 		addresses=""
-		for i in self.sconf.address:
-			addresses = addresses + str(i) + " "
+		if self.sconf.assigntype == AssignType.DHCP:
+			for i in self.sconf.dhcp_format:
+				dhcp_ip = conn.get_dhcp_server_identifier(i)
+				addresses = addresses + str(dhcp_ip) + " "
+		else:
+			for i in self.sconf.address:
+				addresses = addresses + str(i) + " "
+
 		newentry = addresses + "     " + self.constant_name
 		newentry = newentry + " " + self.sconf.hostname
 		self.host_entries.append(newentry)
@@ -436,6 +448,21 @@ class nmConnection():
 	def get_property(self, prop):
 		return self.properties[prop]
 
+	#
+	# Get DHCP server identifier
+	#
+	def get_dhcp_server_identifier(self, dhcp_format):
+		propstr = subprocess.check_output(["nmcli", "-f", dhcp_format, "con", "show", self.ifc.getifcname()])
+		lines = propstr.splitlines()
+		for data in lines:
+			elements = data.decode("utf-8").split()
+			if len(elements) < 2:
+				continue
+			for key in elements:
+				if key == "dhcp_server_identifier":
+					return elements[3]
+		return None
+
 	#
 	# Using this object, run nmcli to update the os so that the
 	# interface represented here is in sync with our desired
@@ -523,7 +550,7 @@ def main():
 	if conn.sync_to_os() == False:
 		sys.exit(1)
 	print("redfish-finder: Adding redfish host info to OS config")
-	svc.update_redfish_info()
+	svc.update_redfish_info(conn)
 	if svc.output_redfish_config() == False:
 		sys.exit(1)
 	print("redfish-finder: Done, BMC is now reachable via hostname redfish-localhost")
